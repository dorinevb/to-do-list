const myList = document.getElementById("to-do-list");

const addToDo = document.getElementById("add-to-do");

const submitButton = document.getElementById("submit-to-do");

function addToList() {
  let newToDo = addToDo.value;
  myList.innerHTML += "<li>" + newToDo + "</li>";
}

submitButton.addEventListener("click", addToList);